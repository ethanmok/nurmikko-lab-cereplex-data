# nurmikko-lab-cereplex-data

OOK data from the Blackrock Microsystems Cereplex-W. Collected in the Nurmikko Lab, Brown University
Recorded with the Rincon Raptor at 61.44 MSa.

This data is released to the public under the CDLA Permissive License, found at https://cdla.io/permissive-1-0/